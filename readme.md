T-Split
=======
_Traffic capture and splitting script_

## Usage

Server configuration are stored in `config.conf`

Init some semi-default folders:

    mkdir /tmp/captures
    mkdir /tmp/flows


Retrieve current capture from server:

    ./synch.sh /tmp/captures/ /tmp/captures/

Dissect and manually analyze flows of latest captured pcap of a given service:

    ./analyze_latest.sh SERVICEPORT

Dissectet traffic should be saved in pcap files in `/tmp/flows/$SVCPORT` with this file name convention:

    <TIMESTAMP>,<SIP>,<SPORT>,<DIP>,<DPORT>

__Assumption: we assume that services are firstly contacted by remote clients. So consider the DESTIP and the DESTPORT as SERVICEIP and SERVICEPORT.__


Dissection tool:

    ./dissect.sh PCAPFILE OUTPUTFOLDER
    ./dissect.sh PCAPFILE /tmp/flows

It outputs a JSON strings like that:

    [
    {"source_ip":"192.168.0.2","source_port":"","dst_ip":"74.125.224.112","dst_port":"80","ts":"2013-09-21 21:46:12.542464","pcap":"/tmp/flows/80/2013-09-21 21:46:12.542464,192.168.0.2,,74.125.224.112,80" }
    {"source_ip":"192.168.0.2","source_port":"","dst_ip":"74.125.224.87","dst_port":"80","ts":"2013-09-21 21:46:16.025563","pcap":"/tmp/flows/80/2013-09-21 21:46:16.025563,192.168.0.2,,74.125.224.87,80" }
    ]

## SSH

```
cat ~/.ssh/config                                                                                                                                                                                          !1924
host 10.14.100.2
    IdentityFile ~/tmp/ictf-2013/keys/ssh_key

# decrypt ssh key
openssl rsa -in ssh_key -out ssh_key_dec
```

## TODO

* Test submitter with sample flows json
*
