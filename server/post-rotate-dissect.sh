#!/bin/bash
##
## Simple script to enable ./dissect.sh to work as post-rotate command in tcpdump.
## Edit default configuration values by need.
##
## Usage: ./post-rotate-dissect.sh  <CAPTUREFILE.PCAP>
##
## Options:
##      -h, --help Display this message
##
OUTPUTFOLDER='data/flows'  # TODO: configure your output folder here
DISSECT='./dissect.sh'      # TODO: specify your path

INFILE=$1

usage() {
  [ "$*" ] && echo "$0: $*"
  sed -n '/^##/,/^$/s/^## \{0,1\}//p' "$0"
  exit 2
} 2>/dev/null

while [ $# -gt 0 ]; do
    case $1 in
    (-h|--help) usage 2>&1;;
    (--) shift; break;;
    (-*) usage "$1: unknown option";;
    (*) break;;
    esac
done

# Dissect the pcap
$DISSECT -v $INFILE $OUTPUTFOLDER 
# Delete dissected capture file
# rm $INFILE
