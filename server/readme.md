Monitoring traffic
=================

    usage: monitor.sh [OPTIONS] SVCPORT1 .. SVCPORTN
    
    Options:
         -h, --help Display this message
         -v, --verbose Verbose output on STDOUT
         -o, --outfoder Output folder
         -i, --iface Monitoring interface name
         -r, --rotation Capture file rotation in Secs (default 3600).
         -e, --exec  Command to execute after rotation
    
    Dependencies:
    
         tcpdump

Example:

    mkdir /tmp/captures
    ./monitor.sh -i eth1 -o /tmp/captures -e "rm $1" -r 300 SERVICE_PORTS

Where 300 means 5 minutes (in seconds)

