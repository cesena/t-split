#!/bin/bash
##
## Start monitoring specified services
#
## Usage: monitor.sh [OPTIONS] SVCPORT1 .. SVCPORTN
##
## Options:
##      -h, --help Display this message
##      -v, --verbose Verbose output on STDOUT
##      -o, --outfoder Output folder
##      -i, --iface Monitoring interface name
##      -r, --rotation Capture file rotation in Secs (default 3600).
##      -e, --exec  Command to execute after rotation
## 
## Dependencies:
##
##      tcpdump
##

startMonitoring(){
  #
  # Start monitoring on specified service ports
  #
  pids=()
  for svc in "$@"
  do
    echo "INFO: Monitoring service on port $svc"
    mkdir "$OUTFOLDER/$svc" 2>/dev/null
    tcpdump $IFACE $ROTATION $EXECCOMMAND -U -s0 -w "$OUTFOLDER/$svc/%Y-%m-%d_%H:%M:%S.pcap" port $svc &
    pids+=($!)
  done
  wait ${pids[@]}
}
control_c()
{
  #
  # Kill all monitoring services 
  #
  echo "INFO: Stopping monitoring on ports ${SERVICES[@]}"
  #kill -TERM -$PID
  exit $?
}
usage() {
  [ "$*" ] && echo "$0: $*"
  sed -n '/^##/,/^$/s/^## \{0,1\}//p' "$0"
  exit 2
} 2>/dev/null


# --------------
# Main
# --------------

OUTFOLDER="."
VERBOSE=0
ROTATION="-G 3600"
EXECCOMMAND=""
PID=$$

while [ $# -gt 0 ]; do
    case $1 in
    (-h|--help) usage 2>&1;;
    (-v|--verbose) VERBOSE=1;shift;;
    (-o|--outfolder) OUTFOLDER="$2";shift;shift;;
    (-i|--iface) IFACE="-i $2";shift;shift;;
    (-r|--rotation) ROTATION="-G $2";shift;shift;;
    (-e|--exec) EXECCOMMAND="-z $2";shift;shift;;
    (--) shift; break;;
    (-*) usage "$1: unknown option";;
    (*) break;;
    esac
done

echo "INFO: Starting Monitoring. Captures in $OUTFOLDER"

if [ $# -gt 0 ]; then
  for i in `seq 0 1 $#`;
  do
    SERVICES[$i]=$1
    shift 1
  done
  echo "INFO: Dumping traffic directed on ports: ${SERVICES[@]}"
  trap control_c SIGINT
  startMonitoring ${SERVICES[@]} 
fi

