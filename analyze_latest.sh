#!/bin/bash
##
## Analyze latest pcap for specified server
##
## Usage:
##
##   ./analyze_latest.sh SERVICEPORT
##

SERVICE=$1

./analyze.sh "/tmp/captures/$SERVICE/$(ls -t -1 /tmp/captures/$SERVICE | head -1)" 2>/dev/null
