#!/bin/bash
##
## Copyright (C) 2013 stk <stk@101337>
## Distributed under terms of the MIT license.
##
## Description:
##
##    Download capture via ssh from remote server
##
## Usage:
##
##    sync OPTIONS LOCALFOLDER REMOTEFOLDER
##
##
## Parameters:
##
##   LOCALFOLDER    The folder where to dump captures
##
##   REMOTEFOLDER   The folder on server where captures are saved
##
## Options:
##
##    -h, --help    This help message
##
## Dependencies:
##
##    * rsync (apt-get install rsync)
##    * sshpass (apt-get install sshpass or http://sourceforge.net/projects/sshpass/)
##

# ----------------
# UTILITY FUNCTIONS
# ----------------

echoerr() {
  echo "$@" 1>&2;
}
usage() {
  [ "$*" ] && echoerr "$0: $*"
  sed -n '/^##/,/^$/s/^## \{0,1\}//p' "$0" 1>&2
  exit 2
}

# ----------------
# FUNCTIONS
# ----------------


# ----------------
# MAIN
# ----------------
# Options parsing
while [ $# -gt 0 ]; do
  case $1 in
    (-h|--help) usage 2>&1;;
    (--) shift; break;;
    (-*) usage "$1: unknown option";;
    (*) break;;
  esac
done

source config.conf

LOCALFOLDER=$1
REMOTEFOLDER=$2         # /tmp/captures
#REMOTEUSER=ictf
#REMOTEHOST=10.14.100.2
REMOTEUSER=$_REMOTEUSER
REMOTEPWD=$_REMOTEPWD
REMOTEHOST=$_REMOTEHOST

rsync -vz -r $REMOTEUSER@$REMOTEHOST:$REMOTEFOLDER $LOCALFOLDER