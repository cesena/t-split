#!/bin/bash
##
## Synch and analyze service
##
##

SVC=$1
SVCPATH="/tmp/captures/$SVC"

echo "Synching.."
./synch.sh /tmp/captures/ /tmp/captures/ 2>/dev/null 

if [ -d "$SVCPATH" ]; then
  ./dissect.sh "$SVCPATH" 2>/dev/null 
else

  echo "Statistics..."
  tshark -r  "/tmp/captures/$SERVICE/$(ls -t -1 /tmp/captures/$SERVICE | head -1)" -q -n -z conv,tcp
  echo "Dissecting..." 
  echo "$(tput setaf 1)Cannot access to service folder: $SVCPATH $(tput sgr0)"
  exit -1
fi

