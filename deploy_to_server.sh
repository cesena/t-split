#!/bin/bash
## deploy_to_server.sh
##
## Copyright (C) 2013 stk <stk@101337>
## Distributed under terms of the MIT license.
##
## Description:
##
##    Deploy server monitoring scripts to server
##
## Usage:
##
##    deploy_to_server OPTIONS PARAMS
##
## Parameters:
##    
##    REMOTEDIR     Remote directory where to deploy server scripts
##
## Options:
##    
##    -h, --help    This help message
##
## Example:
##    
##    deploy_to_server /tmp/monitor
##
## Dependencies:
##     
##    * rsync
##    * sshpass 
##

# ----------------
# UTILITY FUNCTIONS
# ----------------

echoerr() { 
  echo "$@" 1>&2; 
}
usage() {
  [ "$*" ] && echoerr "$0: $*"
  sed -n '/^##/,/^$/s/^## \{0,1\}//p' "$0" 1>&2
  exit 2
}

# ----------------
# FUNCTIONS
# ----------------


# ----------------
# MAIN
# ----------------


# Options parsing
while [ $# -gt 0 ]; do
  case $1 in
    (-h|--help) usage 2>&1;;
    (--) shift; break;;
    (-*) usage "$1: unknown option";;
    (*) break;;
  esac
done

source config.conf

REMOTEFOLDER=$1

rsync -vz -r -e "sshpass -p $_REMOTEPWD ssh" ./server/ $_REMOTEUSER@$_REMOTEHOST:$REMOTEFOLDER 

echo "INFO: monitoring tools deployed in $REMOTEFOLDER"

