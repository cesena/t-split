#! /bin/bash
##
## Description:
##
##   Dissect network traffic from a PCAP file.
##   Extract network bidirectional flows (source ip, source port, dest ip, dest port, timestamp) and
##   store them in separate files.
##
##   It also output flow information in JSON format.
##
## Usage:
##
##    dissect.sh  PCAPFILE OUTPUTFOLDER
##
## Options:
##
##   -h, --help Display this message
##
## Dependencies:
##
##    * tshark
##

echoerr() {
  echo "$@" 1>&2;
}
getCpuNum(){
  CPUS=`grep -c ^processor /proc/cpuinfo`
  echo $CPUS
}
usage() {
  [ "$*" ] && echo "$0: $*"
  sed -n '/^##/,/^$/s/^## \{0,1\}//p' "$0"
  exit 2
} 2>/dev/null
control_c() {
  # run if user hits control-c
  echoerr "WARN: User has interrupted the execution"
  pkill -TERM -P $$
  exit $?
}


function extractFlows(){
	#
	# Dissect the network capture in bidirectional flows
	# (unlike tcpflow which separate in unidirectional flows)
	#
	# Usage: extractFlows <CAPTUREFILE.PCAP> <OUTPUTFOLDER>
	#
  MAXPROCS=4
  NPROCS=0
  PIDS=()
	PCAPFILE=$1
	OUTDIR=$2
	NSTREAMS=`tshark -n -r "$PCAPFILE" -T fields -e tcp.stream 2>/dev/null |tail -1`
  NMINUSONE=`expr $NSTREAMS - 1`
  trap control_c SIGINT

#  echo "[ "
	for i in $(seq 0 1 $NSTREAMS);
	do
		#
		# Assumption: we assume that services are firstly contacted by remote clients.
		#             So consider the $DIP and the $DPORT as SERVICEIP and SERVICEPORT.
		#             We also assume that REMOTE CLIENT ports are 5 digits
		#
		ROW=`tshark -r "$PCAPFILE" -R "tcp.stream eq $i" -tad -o column.format:'"Source", "%s", "Destination", "%d", "srcport", "%uS", "dstport", "%uD","Time","%t"' 2>/dev/null | head -1`

    PORTS=`echo $ROW | sed "s/[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}//g" | sed "s/[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\} [0-9]\{2\}:[0-9]\{2\}:[0-9]\{2\}\.[0-9]\{0,6\}//g" | xargs`
		SPORT=`echo $ROW | cut -d ' ' -f4` #$PORTS | egrep -o "[0-9]{5}" | head -1 | xargs`
		DPORT=`echo $ROW | cut -d ' ' -f5` #$PORTS | egrep -o "[0-9]{1,5}" | tail -1 | xargs`
		SIP=`echo $ROW | cut -d ' ' -f1 `  #egrep -o "[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}" | head -1 | xargs`    #'10.13.38.54'
		DIP=`echo $ROW | cut -d ' ' -f3 `  #egrep -o "[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}" | tail -1 | xargs`    #'10.13.37.54'
		TIMESTAMP=`echo $ROW | egrep -o "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{0,6}" `

    if [ ! -d "$OUTDIR/$DPORT" ]; then
      mkdir "$OUTDIR/$DPORT"
    fi

    OUTFILE="$OUTDIR/$DPORT/$TIMESTAMP,$SIP,$SPORT,$DIP,$DPORT"
    OUTFILE=`python2 -c "print \"$OUTFILE\".strip()"`
    OUTFILE=`python2 -c "import os; print os.path.abspath('$OUTFILE').strip()"`

#    echo -n "{\"source_ip\":\"$SIP\",\"source_port\":\"$SPORT\",\"dst_ip\":\"$DIP\",\"dst_port\":\"$DPORT\",\"ts\":\"$TIMESTAMP\",\"pcap\":\"$OUTFILE\" }"
    echo $OUTFILE
    tshark -r "$PCAPFILE" -R "tcp.stream eq $i" -w "$OUTFILE" &
    PIDS+=("$!")
    ((NPROCS++))

    if [ $NPROCS -ge $MAXPROCS ]; then
      wait ${PIDS[@]}
      PIDS=()
      NPROCS=0
    fi
#    if [ $i -lt $NMINUSONE ]; then
#      echo ","
#    else
#      echo ""
#    fi
  done
#  echo "]"
}

#---------------------
# MAIN
#---------------------

while [ $# -gt 0 ]; do
    case $1 in
    (-h|--help) usage 2>&1;;
    (-v|--verbose) VERBOSE=1;shift;break;;
    (--) shift; break;;
    (-*) usage "$1: unknown option";;
    (*) break;;
    esac
done

source config.conf

if [ $# -eq 2 ]; then
   INFILE=$1
   OUTFOLDER=$2
else
  INFILE=$1
  OUTFOLDER=$_FLOWS_FOLDER
fi

if [ "$INFILE" == "" ] || [ "$OUTFOLDER" == "" ]; then
  echo "$(tput setaf 1)Missing mandatory parameter!$(tput sgr0)"
  exit -1
fi

extractFlows "$INFILE" "$OUTFOLDER"

#
# tshark -r data/logs/capture.pcap -R tcp.stream eq 3 -T fields -E separator=, -e frame.time -e ip.src -e tcp.srcport -e ip.dst -e tcp.dstport -e data
# ...
# Mar 23, 2013 00:23:24.689578000,10.13.38.54,33124,10.13.37.54,4444,373030300a
# Mar 23, 2013 00:23:24.689646000,10.13.37.54,4444,10.13.38.54,33124,426f726f6e206c6576656c20697320746f6f20686967683a203736343020286d61782031303030290a
# ...


