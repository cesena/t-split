#!/bin/bash
##
## Synch and analyze service
##
##

SVC=$1
SVCPATH="/tmp/captures/$SVC"

./synch.sh /tmp/captures/ /tmp/captures/ 

if [ -d "$SVCPATH" ]; then
  ./analyze_latest.sh "$1" 2>/dev/null 
else
  echo "$(tput setaf 1)Cannot access to service folder: $SVCPATH $(tput sgr0)"
  exit -1
fi

