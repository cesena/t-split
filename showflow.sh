#!/bin/bash
## showflow.sh
##
## Copyright (C) 2013 stk <stk@101337>
## Distributed under terms of the MIT license.
##
## Description:
##
##   Conversation visualization tool 
##
## Usage:
##
##    showflow OPTIONS PCAPFILE
##
## Parameters:
##    
##    PCAPFILE    The pcap file containing the conversation
##
## Options:
##    
##    -h, --help    This help message
##
## Dependencies:
##    
##    * tshark
##    * dialog
##

# ----------------
# UTILITY FUNCTIONS
# ----------------

echoerr() { 
  echo "$@" 1>&2; 
}
usage() {
  [ "$*" ] && echoerr "$0: $*"
  sed -n '/^##/,/^$/s/^## \{0,1\}//p' "$0" 1>&2
  exit 2
}

# ----------------
# FUNCTIONS
# ----------------


# ----------------
# MAIN
# ----------------

# Options parsing
while [ $# -gt 0 ]; do
  case $1 in
    (-h|--help) usage 2>&1;;
    (--) shift; break;;
    (-*) usage "$1: unknown option";;
    (*) break;;
  esac
done

source config.conf

if [ "$1" == "" ]; then
  echo "$(tput setaf 1)Missing mandatory parameter!$(tput sgr0)"
  exit -1
fi

#if [ ! -f "$1" ]; then
#  echo "$(tput setaf 1)Cannot open specified file '$1' $(tput sgr0)"
#  exit -1
#fi

PCAPFILE=$1
#if [ "$PCAPFILE" == "" ]; then
#  PCAPFILE=$1
#fi

echo "Loading conversation pcap: $PCAPFILE"
flowname=`basename "$PCAPFILE"| python2 -c "import sys; print '  -  '.join([x for x in sys.stdin.read().split(',')])" ` 
echo "Flowname for manual submission: $flowname"

#sleep 10


TFILE=`tempfile`

echo "$(tput stab 6) File: '$PCAPFILE' $(tput sgr0) " >> $TFILE
echo "$(tput stab 6) Flow: '$flowname' $(tput sgr0) " >> $TFILE
echo "$(tput stab 6) -------------------------------------- $(tput sgr0)" >> $TFILE

tshark -r "$PCAPFILE" -n -x | sed "s/\(^.*10.13.100.2 ->.*\)/$(tput setaf 2)\1$(tput setaf 7)/g" | sed "s/\(^.*10.13.100.1 ->.*\)/$(tput setaf 1)\1$(tput setaf 7)/g" >> $TFILE

cat $TFILE | less -r

if [ $? -eq 0 ]; then 

  #dialog --title "Is it malicious"  --defaultno --clear  --yesno "Would you like to submit flow\n$flowname\nas malicuos?" 10 90
  echo "$(tput sgr0)Would you to submit it as malicious?$(tput sgr0)  $(tput bold)[y/N]$(tput sgr0)"
  #echo -n ">"
  #read res
  #if [ "$res" == "y" ] || [ "$res" == "Y" ]; then
  #if [ $? -eq 0 ]; then
  #  echo "$(tput sgr0)Sending '$flowname' to $_SUBMISSION_HOST $_SUBMISSION_PORT"
  #  #echo "$flowname" | nc $_SUBMISSION_HOST $_SUBMISSION_PORT 
  #fi
else

  echo "$(tput setaf 1)Something has gone wrong during conversation opening$(tput sgr0)"
  exit -2
fi

rm $TFILE

