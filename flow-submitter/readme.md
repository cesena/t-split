Flows Submission Utilities
==========================


__Edit `flow-submit.py` with proper cookies, urls and ports.__

    
Start gathering FlowIDs from remote server and listen for internal submission requests:  

      ./flow-submit.py service 

Perform a quick and dirty submission of a flow:

      ./flow-submit.py direct SIP SPORT DIP DPORT TS

Submission request format:

    SOURCEIP_SOURCEPORT_DESTIP_DESTPORT_TIMESTAMP

    Eg.  
        127.0.0.1_12342_8.8.4.4._80_2013-11-09 17:00:33.847917


Getting flow reqest string from raw fields

    ./Flows.py SIP SPORT DIP DPORT TS


Manually submit some FlowIDs:

    ./FlowSubmitter.py FLOWID1 FLOWID2 ...


Manually get flow information:


    ./FlowGather.py
  
    NOTE: need to edit url and cookies inside this file for using it from shell 

