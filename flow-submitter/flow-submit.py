#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2013 stk <stk@101337>
#
# Distributed under terms of the MIT license.

"""

"""
import argparse
import logging
import threading
import SocketServer
import socket

import FlowSubmitter as fs
import FlowGather as fg
import FlowBuffer as fb 

from FlowSubmitter import FlowSubmitter 
from FlowBuffer import FlowBuffer
from FlowGather import FlowGather


########################################################################################
#Parameters 
########################################################################################

_HOST='0.0.0.0'
_PORT=4444
_GATHERING_INTERVAL=20  # secs
_FLOW_BUFFER_MAX_SIZE=100000

# Where to retrieve flow informations
_NETFLOW_URL= "http://groundhog.ictf2013.net/netflows"
_NETFLOW_COOKIES="cookie1=val,c1=2"

# Where to submit flows
_SUBMIT_URL='http://www.ictf2013.net/submit_netflow'

########################################################################################
# Submission service
########################################################################################

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer): pass

def submit_flow(keystr, flowbuffer, flowsubmitter):
  #
  # Submission function (used also for delayed submissions)
  #
  flowid=None
  submissionSent=False
  try:
    flowbuffer.acquire_read()
    flowid=flowbuffer.get(keystr)
  except KeyError as ke:
    logging.debug("Flow {0} not found!".format(keystr))
    submissionSent=False
  finally:  
    flowbuffer.release()
    if flowid is not None:
       flowsubmitter.submit_flowid(flowid)
       submissionSent=True
    return submissionSent

class FlowSubmissionHandler(SocketServer.StreamRequestHandler):
  #
  # Frontend for handling submission request
  #
  def handle(self):
    global flowsubmitter
    global flowbuffer
    try:
      data=self.rfile.readline().strip()
      while data!=None:
        logging.debug("Serving request for {0}".format(data))
        flowid=None

        try:
          sip,sport,dip,dport,ts=data.split('_')
          keystr="{0}_{1}_{2}_{3}".format(sip, sport, dip, dport)
          submitted=submit_flow(keystr, flowbuffer, flowsubmitter)
          
          if submitted is False:
            gatherer=threading.Timer(60, submit_flow, [keystr, flowbuffer, flowsubmitter])
            gatherer.start()
            self.wfile("Flowid not yet present submission delayed (60sec)")
            logging.debug("delaying submission of {0} (60sec)".format(keystr))
          else:
            self.wfile.write("Flowid {0} sent to submitter\n".format(flowid))
        
        except:
          self.wfile.write("Unknown format\n")

        data=self.rfile.readline().strip()
    except Exception as e:
      logging.error("Something wrong during submission request handling ({0})".format(e))

########################################################################################
# MAIN
########################################################################################

def start_submission_service():
  global flowsubmitter
  global flowbuffer
 

  logging.debug("Initializing FlowBuffer and FlowSubmitter")
  flowbuffer = FlowBuffer(max_size=_FLOW_BUFFER_MAX_SIZE)
  flowsubmitter = FlowSubmitter(submit_url=_SUBMIT_URL)

  logging.info("Starting gathering service")
  gathservice = FlowGather( gath_period=_GATHERING_INTERVAL, flow_buffer=flowbuffer, netflow_url=_NETFLOW_URL, netflow_cookies=_NETFLOW_COOKIES)
  gathservice.setDaemon(True)
  gathservice.start()
  
  logging.info("Starting internal submission service listener on {0}".format((_HOST,_PORT)))
  server = SocketServer.TCPServer((_HOST, _PORT), FlowSubmissionHandler)
  submservice=threading.Thread(target=server.serve_forever)
  submservice.setDaemon(True)
  submservice.start()

  gathservice.join(timeout=9999999999)
  submservice.join(timeout=9999999999)

def quick_submit(flowtuples):
  #
  # Single flow tuple example (sip,sport,dip,dport,ts)
  #
  flows=fg.get_json_flows(_NETFLOW_URL, _NETFLOW_COOKIES)
  flowids=[]
  for ft in flowtuples:
    for f in flows:
      #  sip,                            sport,                           dip,                            dport
      if f[1].strip()==ft[0].strip() and f[2].strip()==ft[1].strip() and  f[3].strip()==ft[2].strip() and f[4].strip()==ft[3].strip():      
        flowids.append(f[0])  # flowid
        break
  fs.submit_flows(flowids,submit_url=_SUBMIT_URL)


def main():
  # Default timeout for sockets
  import socket
  socket.setdefaulttimeout(10)
  
  parser = argparse.ArgumentParser(description='Internal submission script and service')
  subparsers = parser.add_subparsers()
  add_s = subparsers.add_parser('service')
  add_s.set_defaults(which='service') 
  add_d = subparsers.add_parser('direct')
  
  add_d.add_argument_group('direct')
  add_d.set_defaults(which='direct') 
  add_d.add_argument('sip', metavar='SOURCEIP', help='Source IP')
  add_d.add_argument('sport', metavar='SOURCEPORT', help='Source port')
  add_d.add_argument('dip', metavar='DESTIP', help='Dest IP')
  add_d.add_argument('dport', metavar='DESTPORT', help='Dest port')
  add_d.add_argument('ts', metavar='TIMESTAMP', nargs='?', help='Timestamp')
  args = parser.parse_args()

  if args.which is 'service':
    start_submission_service()
  elif args.which is 'direct':
    quick_submit([(args.sip,args.sport,args.dip,args.dport,args.ts)])

if __name__=="__main__":
  logging.basicConfig(level=logging.DEBUG) #filename='flowsubmitter.log',
  main()

