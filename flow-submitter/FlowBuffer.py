"""

"""
import logging

from rwlock import RWLock


class FlowBuffer:
  #
  # FlowBuffer data structure
  #
  def __init__(self, max_size=1000000):
    self.lock=RWLock() 
    self.maxsize=max_size
    self.asdict=dict()
    self.aslist=list()
  
  def _discard_some(self):
    #
    # Discard some old data
    #
    while len(self.aslist)>self.maxsize*0.70:
      k=self.aslist[0]  # pick the older elements
      self.remove(k)
      del self.asdict[k]
  
  def acquire_write(self):
    self.lock.acquire_write()
  
  def acquire_read(self):
    self.lock.acquire_read()
  
  def release(self):
    self.lock.release_write()
  
  def get(self, key):
    #
    # Get a value (using a STRING key)
    #
    k=hash(key)
    return self.asdict[k]
  
  def pop(self, key):
    #
    # Pop a value from buffer (using a STRING key)
    #
    k=hash(key)
    k=hash(key)
    v=self.asdict.pop(k)
    self.aslist.remove(k)
    return v

  def put(self, key, value):
    #
    # Put it inside! (using a STRING key)
    #
    k=hash(key)
    if len(self.aslist)>self.maxlen:
      self._discard_some()
    self.asdict[k]=value
    self.aslist.append(k)
