#!/usr/bin/env python2
"""
  Flow submitter class
"""
import argparse
import mechanize
import cookielib
import logging

_SUBMIT_URL='http://www.ictf2013.net/submit_netflow'

class FlowSubmitter:
  #
  # HTTP flow submitter
  #
  def __init__(self, submit_url=_SUBMIT_URL):
    self.submit_url=submit_url
    self.br=mechanize.Browser()
    self.cj=cookielib.LWPCookieJar()
    self.br.set_cookiejar(self.cj)
    self.br.set_handle_equiv(True)
    self.br.set_handle_gzip(True)
    self.br.set_handle_redirect(True)
    self.br.set_handle_referer(True)
    self.br.set_handle_robots(False)
    self.br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
    self.br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
  
  def setup_cookies(self):
    #
    # Setup cookies for authenticate the session
    #
    logging.info("Setting up FlowID submitter cookies")
    ck = cookielib.Cookie(version=0, name='remember_token', 
                          value='WyI1OSIsIjE0OTUyMTAyOGZlNzZiY2U1ZjgzMzVhNDE4MDMxMDRmIl0.BC4Z7w.n8hDh1io5LmpJg2wnEiM2Uy_UMA', port=None, 
                          port_specified=False, domain='www.ictf2013.net', 
                          domain_specified=False, domain_initial_dot=False, path='/', 
                          path_specified=True, secure=False, expires=None, discard=True, 
                          comment=None, comment_url=None, rest={'HttpOnly': None}, rfc2109=False)
    self.cj.set_cookie(ck)
  
  def submit_flowid(self, flowid):
    #
    # Submit a Flow ID
    #
    logging.info("Submitting flow {0}".format(flowid))
    self.br.open(self.submit_url) 
    self.br.select_form(nr=0)
    self.br.form['netflow_id']=flowid
    self.br.submit()
    res= self.br.response()
    if res is not None:
      logging.debug(self.br.response().read()) 
      # TODO: filter response
    else:
      logging.error("Could not submit flow {0}".format(flowid))

def submit_flows(flows, submission_url=_SUBMIT_URL):
  #
  # Submit som flows 
  #
  s=FlowSubmitter(submission_url)
  s.setup_cookies()
  for f in flows:
    s.submit_flowid(f)

def main():
  # Default timeout for sockets
  import socket
  socket.setdefaulttimeout(10)
  
  parser = argparse.ArgumentParser(description='Submit a FlowID')
  parser.add_argument('flowids', metavar='FLOWID', type=int, nargs='+',
                     help='FlowIDs to submit')
  args = parser.parse_args()
  submit_flows(args.flowids)
  
if __name__=="__main__":
  main()
