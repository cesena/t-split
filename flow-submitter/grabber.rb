##
# iCTF netflow grabber and matcher
#
# Authors: LMOLR, STK
#

require 'json'
require 'time'
require 'fileutils'

caps_dir = './data/flows'         # Folder containing flows captures using file name convention:  <SRCIP> <SRCPORT> <DSTIP> <DSTPORT> [<TIMESTAMP>]
out_dir = './data/output'         # Output folder, will be structured like that: <out_dir>/<service_port>/<flowid>
curl_cmd = "curl -b cookie.txt http://groundhog.ictf2013.net/netflows"

##
# Utility functions
#

def sanitize_ip(ip)
  #
  # Sanitize an ip string
  #
  ip.split('.').collect do |comp|
    ('0' * (3 - comp.length)) << comp
  end.join('.')
end

def sanitize_port(port)
  #
  # Sanitize a port string
  #
  ('0' * (3 - port.length)) << port
end

#####################
# MAIN
#####################

# Scraping flow data from ctf server
raw_out = `#{curl_cmd}`
out = JSON.load(raw_out)

unless out['netflows'] then 
  puts "ERR: Something wrong with netflows scraping"
  exit 
end

flows = out['netflows']
puts "INFO: #{flows.length} flows detected"

flows.each do |f|
  # Recreate the flow file name from flow data
  # File name convention:  <SRCIP> <SRCPORT> <DSTIP> <DSTPORT> [<TIMESTAMP>]
  file_name = "#{sanitize_ip(f['source_ip'])} #{f['source_port']} #{sanitize_ip(f['dest_ip'])} #{f['dest_port']}"
  # This is the flow file
  in_file = "#{caps_dir}/#{file_name}"
  # Create a separate folder for each service, just in case
  out_dir_full = "#{out_dir}/#{f['dest_port']}"
  FileUtils.mkdir_p(out_dir_full)
  # New flow name is the id
  out_file = "#{out_dir_full}/#{f['id']}"

  # Match flow name, ignore timestamp. It seems to be a good approximation
  Dir.glob("#{in_file}*") do |f1|
	puts f1
    	File.write(out_file, File.read(f1))
  end
  
  puts "DEBUG: flow #{f['id']} from to #{out_file}"
end


##
# Version with timestamp matching (too hard to match due to local time issues)
#
=begin
if out['netflows']
  flows = out['netflows']
  
  puts "#{flows.length} flows"
  puts "#{netflow_pth}_#{Time.now.utc.strftime("%m_%d_%H.%M.%S.%L")}"

  File.open("#{netflow_pth}_#{Time.now.utc.strftime("%m_%d_%H.%M.%S.%L")}", 'w') do |f|

    flows.each do |flow|
      puts "ID: #{flow['id']}"
      file_pth = "#{base_cap_pth}/#{flow['source_ip']}#{flow['source_port']}_#{flow['dest_ip']}#{flow['dest_port']}"
      puts "SEARCHING: #{file_pth}"

      line = [:id, :source_ip, :source_port, :dest_ip, :dest_port, :created_timestamp].collect do |k|
        if (k == :created_timestamp)
          Time.parse(flow[k.to_s]).to_s
        else
          flow[k.to_s]
        end
      end.join(',')

      f.write("#{line}\n")
    end
  end # file.new
end

puts 'done'
=end
