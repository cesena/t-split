import logging
import argparse
import threading
import urllib2
import json
import cookielib
import time

from Flows import get_key_string
from Flows import get_flow_tuple

_NETFLOW_URL= "http://groundhog.ictf2013.net/netflows"
_NETFLOW_COOKIES="cookie1=val,c1=2"

def curl_get(url,cookies='sample1=v1,sample2=v2'):
  #
  # Utility function for retrieving a page
  #
  opener = urllib2.build_opener()
  opener.addheaders.append(('Cookie', cookies ))
  f=opener.open(url,timeout=10)
  if f.getcode()==200:
    return f.read()
  else:
    logging.error("retrieving {0} has returned {1}".format(_SERVER_URL, f.getcode()))
    return ""

def get_json_flows(flow_url, flow_cookies):
  jsonflows=curl_get(flow_url,flow_cookies)
  flows=json.loads(jsonflows)
  self.flow_buffer.acquire()
  for f in flows:
     # 
     # !!! EDIT HERE FOR PROPER JSON FIELD INTERPRETATION !!! 
     #
     yield get_flow_tuple(f) 

class FlowGather(threading.Thread):
  #
  # Periodic FlowInfo gathering thread
  #
  def __init__(self, gath_period, flow_buffer, netflow_url, netflow_cookies):
    threading.Thread.__init__(self)
    self.flow_buffer=flow_buffer
    self.gath_period=gath_period
    self.netflow_url=netflow_url
    self.netflow_cookies=netflow_cookies

  def task_gather_netflow_info(self):
    #
    # Flow information gathering task
    #
    try: 
      jsonflows=curl_get(self.netflow_url,cookies=self.netflow_cookies)
      if jsonflows=="":
        return
      flows=json.loads(jsonflows)
      logging.info("{0} flows info gathered".format(len(flows)))
    except Exception as e:
      logging.error("Something wrong ({0})".format(str(e)))
      return
    try: 
      self.flow_buffer.acquire()
      for f in flows:
        # 
        # !!! EDIT HERE FOR PROPER JSON FIELD INTERPRETATION !!! 
        #
        ft=get_flow_tuple(f)
        keystr=get_key_string(ft[1], ft[2], ft[3], tf[3])
        self.flow_buffer.put(keystr, ft[0])
    except Exception as e:
      logging.error("Something wrong ({0})".format(str(e)))
    finally:
        self.flow_buffer.release()
    return

  def run(self):
    #
    # Run periodically
    #
    while True:
      try:
        self.task_gather_netflow_info()
      except Exception as e:
        logging.error("Something wrong during gathering ({0})".format(str(e)))
      finally:
        time.sleep(self.gath_period)


def main():
  parser = argparse.ArgumentParser(description='Retrieve Flow information')
  print curl_get(_NETFLOW_URL, _NETFLOW_COOKIES) 

if __name__=="__main__":
  main()
