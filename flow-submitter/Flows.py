#!/usr/bin/env python2
"""

"""
import argparse



def get_flow_tuple(json_flow_object):
  #
  # !!! TODO EDIT HERE !!!
  #
  flowid=f['id']
  sip=f['source_ip']
  sport=f['source_port']
  dip=f['dest_ip']
  dport=f['dest_port']
  ts=f['timestamp']
  return (flowid, sip, sport, dip, dport, ts) 

def get_flow_dict(json_flow_object):
  ft= get_flow_tuple(json_flow_object)
  return {'id':ft[0], 'sip':ft[1], 'sport':ft[2], 'dip':ft[3], 'dport':ft[4], 'ts':ft[5] }


def get_key_string(source_ip, source_port, dest_ip, dest_port, timestamp=None):
  keystr=None
  if timestamp==None:
    keystr="{0}_{1}_{2}_{3}".format(source_ip, source_port, dest_ip, dest_port)
  else:
    raise Exception("Sorry, not yet implemented !!!")
  return keystr


def main():
  parser = argparse.ArgumentParser(description='Obtain a flow key string that can be used in internal submission service')
  parser.add_argument('sip', metavar='SOURCEIP', help='Source IP')
  parser.add_argument('sport', metavar='SOURCEPORT', help='Source port')
  parser.add_argument('dip', metavar='DESTIP', help='Dest IP')
  parser.add_argument('dport', metavar='DESTPORT', help='Dest port')
  parser.add_argument('ts', metavar='TIMESTAMP', nargs='?', help='Timestamp')
  args = parser.parse_args()
  print get_key_string(*[x for x in args.__dict__.values()])


if __name__=="__main__":
  main()
