#!/bin/bash
## analyze.sh
##
## Copyright (C) 2013 stk <stk@101337>
## Distributed under terms of the MIT license.
##
## Description:
##
##    NA
##
## Usage:
##
##    analyze OPTIONS PARAMS
##
## Parameters:
##
##    NA
##
## Options:
##
##    -h, --help    This help message
##
## Dependencies:
##
##    * tshark
##    * showflow.sh
##

# ----------------
# UTILITY FUNCTIONS
# ----------------

echoerr() {
  echo "$@" 1>&2;
}
usage() {
  [ "$*" ] && echoerr "$0: $*"
  sed -n '/^##/,/^$/s/^## \{0,1\}//p' "$0" 1>&2
  exit 2
}

# ----------------
# FUNCTIONS
# ----------------


# ----------------
# MAIN
# ----------------

# Options parsing
while [ $# -gt 0 ]; do
  case $1 in
    (-h|--help) usage 2>&1;;
    (--) shift; break;;
    (-*) usage "$1: unknown option";;
    (*) break;;
  esac
done

if [ "$1" == "" ]; then
  echo "$(tput setaf 1)Missing mandatory parameter!$(tput sgr0)"
  exit -1
fi

if [ ! -f "$1" ]; then
  echo "$(tput setaf 1)Cannot open specified file: $1 $(tput sgr0)"
  exit -2
fi

source config.conf

PCAPFILE=$1
FLOWSFOLDER=$_FLOWS_FOLDER

while read pcap;
do
   ./showflow.sh "$pcap" 2>/dev/null
done < <(./dissect.sh "$PCAPFILE" ) #| python2 -c "import json,sys; print '\n'.join([ x['pcap'] for x in json.loads(sys.stdin.read())])")


